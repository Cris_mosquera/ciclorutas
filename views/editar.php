<?php 
    require '../models/connection.php';
    require '../models/models.php';
    $mysqli = conexion();
    $id = $_GET['id'];
    $sqlTypes  =  viewBicit($id);
    $sqlDatas= $mysqli->query($sqlTypes);
    $sqlType =  viewTypeBici();
    $sqlData = $mysqli->query($sqlType);
?>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<div class="card">
    <div class="card-header">
        <div class="text-center">
            <h5>Editar Bicicletas</h5>
        </div>
    </div>
<form method="POST" action="../models/edit.php">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <?php while($fila = $sqlDatas->fetch_array()){ ?>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="text" name="nombre" class="form-control form-control-sm" placeholder="Nombre Bicicleta" required maxlength="12" value="<?php echo $fila["nombre"]; ?>">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="text" name="desc" class="form-control form-control-sm" placeholder="Precio Alquiler" required maxlength="500" value="<?php echo $fila["precio_alquiler"]; ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <select class="form-control form-control-sm" name="tipo" required>
                        <option value="0"><?php echo $fila["des"]; ?></option>
                        <?php while($filas = $sqlData->fetch_array()){ ?>
                        <option value="<?php echo $filas['idtipobicicleta']; ?>">
                            <?php echo $filas['descripcion']; ?>
                        </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="color" name="color" class="form-control form-control-sm" required value="<?php echo $fila["color"]; ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <textarea placeholder="Descripcion" name="des" class="form-control form-control-sm" required maxlength="500">
                    <?php echo $fila["descripcion"]; ?>
                    </textarea>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success btn-sm">
                        Edit
                    </button>
                </div>
            </div>
        </div>
    </div>
 </form>
</div>
<?php } ?>