var app = angular.module('myApp', ['ngRoute']);

//RUTAS
app.config(['$routeProvider',function($routeProvider){

		$routeProvider.when('/bicicletas',{
			templateUrl:'views/bici.php'
		}).when('/bicicletasCrear',{
			templateUrl:'views/bici_crear.php'
		}).when('/editar',{
			templateUrl:'views/editar.php'
		}).when('/verDoc',{
			templateUrl:'views/verDoc.php'
		}).when('/enviarDatos',{
			templateUrl:'models/subirFicheros.php'
		}).otherwise({
			redirectTo:'/'
		});
}]);



